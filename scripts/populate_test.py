import json

from scrapy import log

from redis import StrictRedis

import settings

from items import AppdexItem

from pprint import pprint

import sys

if __name__ == '__main__':
	red = StrictRedis()

	filename = sys.argv[1]

	with open(filename, 'r') as f:
		jitems = json.load(f)
		
# 		pprint(jitems)
		
		for ji in jitems:
			i = AppdexItem()
			for k in ji.iterkeys():
				i[k] = ji[k]
				pass
			
			print(i)
			red.publish(settings.REDIS_CHANNEL, json.dumps(dict(i)))
			log.msg("Pipeline: item published '%s'" % i.get('name'),
				level=log.WARNING)
			pass
		pass
	
	pass
