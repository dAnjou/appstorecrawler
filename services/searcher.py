#!/usr/bin/env python

from whoosh.index import exists_in, open_dir
from whoosh.qparser import QueryParser

from whoosh import query
from whoosh import classify

from appstorecrawler import settings
from appstorecrawler.models import db_session as db
from appstorecrawler.models import App, CalculatedSimilarity

def log_hit(hit):
    print (hit['name'],
           hit['storeId'],
           hit['producerName'],
           hit.score)


def more_like_this_query(hit, fieldname):
    text = None

    s = hit.searcher

    numterms = 5
    model = classify.Bo1Model
    normalize = False
    docnum = hit.docnum

    if text:
        kts = s.key_terms_from_text(fieldname, text, numterms=numterms,
                                    model=model, normalize=normalize)
    else:
        kts = s.key_terms([docnum], fieldname, numterms=numterms,
                          model=model, normalize=normalize)
        # Create an Or query from the key terms
    q = query.Or([query.Term(fieldname, word, boost=weight)
                  for word, weight in kts])

    return q


def normilize_suggestions(suggestions):
    if suggestions:
        best = suggestions[0].score
        ss = []
        for s in suggestions:
            s.score /= best
            ss.append(s)
            pass
        suggestions = ss
    return suggestions


def suggest(hit):
    # return hit.more_like_this("desc", numterms = 5)

    s = hit.searcher

    # pprint(hit)

    q_desc = more_like_this_query(hit, 'desc').with_boost(0.1)
    q_title = more_like_this_query(hit, 'name')
    q_store = query.Term('storeId', hit['storeId'].lower())
    q_developer = more_like_this_query(hit, 'producerName')

    q = (q_title | q_desc)
    # q = q_desc
    q = q | q_developer
    q = query.AndMaybe(q, query.Not(q_store))

    top = 10
    docnum = hit.docnum

    suggestions = s.search(q, limit=top, mask=set([docnum]))
    # suggestions = s.search(q, limit=top, terms=True)

    suggestions = normilize_suggestions(suggestions)

    # if suggestions.has_matched_terms():
    # # What terms matched in the results?
    #     print(suggestions.matched_terms())
    #
    #     # What terms matched in each hit?
    #     for h in suggestions:
    #         print(h.matched_terms())

    return suggestions


def search(storedir="/tmp/testindex"):
    ix = None
    if exists_in(storedir, indexname="app_index"):
        ix = open_dir(storedir, indexname="app_index")
    else:
        print "no index found"
        return
    with ix.searcher() as searcher:
        rows = db.query(App).all()
        for row in rows:
            parser = QueryParser('appId', ix.schema)
            query = parser.parse(u'%s' % row.appIdInStore)
            results = searcher.search(query)
            for hit in results:
                print '================================================='
                log_hit(hit)
                print "\t## more like this:"
                mls = suggest(hit)
                for subhit in mls:
                    log_hit(subhit)
                    subhit_id = db.query(App.id).filter_by(appIdInStore=subhit['appId']).first()
                    if not subhit_id:
                        print subhit['name'] + " not found in db"
                        continue
                    fId, sId = sorted([subhit_id, row.id])
                    x = db.query(CalculatedSimilarity).filter_by(firstAppId=fId, secondAppId=sId).first()
                    if not x:
                        x = CalculatedSimilarity(fId, sId, subhit.score)
                        db.add(x)
                    else:
                        x.similarityScore = subhit.score
                db.commit()


if __name__ == '__main__':
    search(storedir=settings.config['INDEX_STORE_DIR'])
