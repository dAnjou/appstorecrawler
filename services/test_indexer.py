#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging
import sys
import json

from whoosh.index import create_in, exists_in, open_dir

from appstorecrawler.services.AppSchema import AppSchema
from appstorecrawler.services.indexer import on_items
from appstorecrawler import settings

if __name__ == '__main__':
    store_dir = settings.config['INDEX_STORE_DIR']
    logging.basicConfig(level=logging.DEBUG)

    # setting up whoosh
    ix = None
    if not os.path.isdir(store_dir):
        os.mkdir(store_dir)
    if exists_in(store_dir, indexname="app_index"):
        ix = open_dir(store_dir, indexname="app_index")
    else:
        ix = create_in(store_dir, AppSchema, indexname="app_index")

    filename = sys.argv[1]

    with open(filename, 'r') as f:
        items = json.load(f)

    on_items(items, ix)



