from whoosh import analysis
from whoosh.analysis import StemmingAnalyzer, StandardAnalyzer
from whoosh.fields import SchemaClass, TEXT, KEYWORD, ID

__author__ = 'pavel'



producer_stoplist = analysis.STOP_WORDS | frozenset(('gmbh', 'inc'))
# producer_stoplist = analysis.STOP_WORDS

class AppSchema(SchemaClass):

    name = TEXT(stored=True)
    # url = TEXT(stored=True)
    desc = TEXT(stored=True, vector=True, analyzer=StemmingAnalyzer())
    # price = TEXT()
    # categoryId = TEXT()
    categoryName = KEYWORD(stored=True)
    # appIconLink = TEXT()
    appId = ID(stored=True, unique=True)
    # appScreenshots = TEXT()
    # numberOfStars = TEXT()
    # producerId = TEXT()
    producerName = TEXT(stored = True, analyzer=StandardAnalyzer(stoplist=producer_stoplist))
    storeId = TEXT(stored = True)

