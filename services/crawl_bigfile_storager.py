import sys
import json
import logging

from sqlalchemy.exc import DataError

from appstorecrawler.models import db_session as db
from appstorecrawler.models import Store
import time

from storager import store_item


class Timer:
    def __init__(self):
        self.start = time.clock()

    def now(self, desc=''):
        end = time.clock()
        int = end - self.start
        logging.info('{0} {1}'.format(int, desc))
        self.start = time.clock()

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    #insert app stores if not present
    if db.query(Store).count() < 3:
        playStore = Store("Play Store", "GOOG")
        appStore = Store("AppStore", "AAPL")
        windowsStore = Store("Windows Phone Store", "MSFT")
        db.add_all([playStore, appStore, windowsStore])
        db.commit()

    filename = sys.argv[1]

    with open(filename, 'r') as f:

        i = 0
        items = []

        timer = Timer()

        for line in f:
            line = line.strip()[:-1]
            try:
                store_item(json.loads(line), db)
            except DataError:
                pass

            i += 1
            if i % 2048 == 0:
                db.commit()
                timer.now('commit')
                print i

            pass
        db.commit()
        timer.now('commit')
        print i
    db.close()
