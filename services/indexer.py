#!/usr/bin/env python

import sys
import os
import json
from signal import signal, SIGTERM
import atexit
import logging

from redis import StrictRedis

from whoosh.fields import SchemaClass, TEXT, ID
from whoosh.analysis import StemmingAnalyzer
from whoosh.index import create_in, exists_in, open_dir

from appstorecrawler.services.AppSchema import AppSchema
from appstorecrawler import settings


def write_item(item, schema, writer):
    it = {field: item[field] for field in schema.names()}
    writer.add_document(**it)
    # writer.update_document(**it)
    logging.debug("Added document for '%s'" % str(it))


def on_item(item, ix):
    schema = ix.schema
    writer = ix.writer()
    write_item(item, schema, writer)
    writer.commit()


def on_items(items, ix):
    schema = ix.schema
    writer = ix.writer()

    for item in items:
        write_item(item, schema, writer)

    writer.commit()


if __name__ == '__main__':
    store_dir = setting.config['INDEX_STORE_DIR']
    logging.basicConfig(level=logging.DEBUG)

    # setting up whoosh
    ix = None
    if not os.path.isdir(store_dir):
        os.mkdir(store_dir)
    if exists_in(store_dir, indexname="app_index"):
        ix = open_dir(store_dir, indexname="app_index")
    else:
        ix = create_in(store_dir, AppSchema, indexname="app_index")

    # closing writer on SIGTERM
    def handler():
        logging.info("Stopped listening.")

        writer = ix.writer()
        writer.commit()

    signal(SIGTERM, lambda signum, stack_frame: sys.exit(1))
    atexit.register(handler)

    # setting up redis pubsub
    red = StrictRedis()
    pubsub = red.pubsub()
    pubsub.subscribe('appdex')

    # listen for items
    logging.info("Listen for items ...")
    for item in pubsub.listen():
        # logging.debug("Added document for '%s'" % str(item))
        if item.get('type') == 'message':
            app = json.loads(item.get('data'))
            on_item(app, ix)
