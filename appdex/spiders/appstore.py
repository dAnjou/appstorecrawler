from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule

from appdex.items import AppdexItem

from urlparse import urlparse, urlunparse, parse_qsl
from urllib import urlencode

import re
import json


def kick_query_components(url, exceptions=[]):
    r = urlparse(url)
    qsl = parse_qsl(r[4])
    query = urlencode([q for q in qsl if q[0] in exceptions])
    return urlunparse((r[0], r[1], r[2], "", query, ""))


class Appstore(CrawlSpider):
    name = 'appstore'
    allowed_domains = ['itunes.apple.com']
    start_urls = ['https://itunes.apple.com/us/genre/ios/id36']

    rules = (
        Rule(SgmlLinkExtractor(allow=r'/us/app/'),
             callback='parse_app', follow=True),
        Rule(SgmlLinkExtractor(allow=r'/us/genre/ios')),
    )

    def parse_app(self, response):
        hxs = HtmlXPathSelector(response)
        i = AppdexItem()
        i['storeId'] = 'AAPL'
        i['url'] = kick_query_components(response.url)
        # find app name
        name = hxs.select("//div[@id='title']/div[1]/h1/text()")
        i['name'] = name.extract()[0]
        # find app description
        desc = hxs.select("//div[@id='content']/div/div[2]/div[1]/p")
        i['desc'] = "".join(desc.select("*|text()").extract())
        # find price of app (0.00 of for free)
        price = hxs.select("//div[@class='price']/text()").extract()[0].split()[0]
        price = str(float(price) if self.is_number(price) else 0.00)
        price = price.replace(',','.')

        i['price'] = price
        # find id of category
        categoryIdElement = hxs.select("//li[@class='genre']/a/@href").extract()[0]
        categoryId = re.search("d[0-9][0-9]*", categoryIdElement).group(0)[1:]
        i['categoryId'] = categoryId
        # find name of category
        categoryName = hxs.select("//li[@class='genre']/a/text()").extract()[0]
        i['categoryName'] = categoryName
        # find name of category
        appIconLink = hxs.select("//div[@class='lockup product application']/a[1]/div[@class='artwork']/img/@src").extract()[0]
        i['appIconLink'] = appIconLink
        # find app id
        appId = re.search("d[0-9][0-9]*", response.url).group(0)[1:]
        i['appId'] = appId
        # find app screenshots
        appScreenshots = hxs.select("//div[@class='lockup']/img/@src").extract()
        i['appScreenshots'] = appScreenshots
        # find rating
        fullStars = hxs.select("//div[@class='extra-list customer-ratings'][1]//div[@class='rating'][1]//span[@class='rating-star']").extract()
        halfStars = hxs.select("//div[@class='extra-list customer-ratings'][1]//div[@class='rating'][1]//span[@class='rating-star half']").extract()
        numberOfStars = len(fullStars)
        if len(halfStars) > 0:
            numberOfStars =+ 0.5
        i['numberOfStars']= str(numberOfStars)

        producerId = hxs.select("//a[@class='view-more']/@href").extract()[0]
        producerId = re.search("d[0-9][0-9]*", producerId).group(0)[1:]
        i['producerId'] = producerId
        producerName = hxs.select("//div[@id='title']//h2/text()").extract()[0]
        producerName = producerName.split(' ',1)[1]
        i['producerName'] = producerName

        return i

    @staticmethod
    def is_number(s):
        try:
            float(s)
            return True
        except ValueError:
            return False
