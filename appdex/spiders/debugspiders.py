from scrapy.spider import BaseSpider

from appdex.spiders import appstore, playstore, windowsphonestore


apps = {
    "Angry Birds": {
        "appstore": "https://itunes.apple.com/us/app/angry-birds/id343200656",
        "playstore": "https://play.google.com/store/apps/details?id=com.rovio.angrybirds",
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/angry-birds/e4571a02-0b87-e011-986b-78e7d1fa76f8"
    },
    "Temple Run": {
        "appstore": "https://itunes.apple.com/us/app/temple-run/id420009108",
        "playstore": "https://play.google.com/store/apps/details?id=com.imangi.templerun&hl=en"
    },
    "Minecraft": {
        "appstore": "https://itunes.apple.com/us/app/id479516143",
        "playstore": "https://play.google.com/store/apps/details?id=com.mojang.minecraftpe&hl=en"
    },
    "The Sims 3": {
        "appstore": "https://itunes.apple.com/us/app/the-sims-3/id317904170",
        "playstore": "https://play.google.com/store/apps/details?id=com.eamobile.sims3_na_qwf&hl=en",
        "windowsphonestore" : "http://www.windowsphone.com/en-us/store/app/the-sims-3/9995f674-0ad7-df11-a844-00237de2db9e"
    },
    "IM+ PRO": {
        "appstore": "https://itunes.apple.com/us/app/id296246130",
        "playstore": "https://play.google.com/store/apps/details?id=de.shapeservices.implusfull&hl=en",
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/im-pro/86a45097-5bdb-df11-a844-00237de2db9e"
    },
    "IM+" : {
        "appstore": "https://itunes.apple.com/us/app/im/id285688934",
        "playstore": "https://play.google.com/store/apps/details?id=de.shapeservices.impluslite&hl=en",
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/im/7c59963c-ddae-e011-a53c-78e7d1fa76f8"
    },
    "HootSuite": {
        "appstore": "https://itunes.apple.com/us/app/hootsuite-for-twitter/id341249709",
        "playstore": "https://play.google.com/store/apps/details?id=com.hootsuite.droid.full&hl=en"
    },
    "Facebook": {
        "appstore": "https://itunes.apple.com/us/app/facebook/id284882215",
        "playstore": "https://play.google.com/store/apps/details?id=com.facebook.katana&hl=en"
    },
    "G+": {
        "appstore": "https://itunes.apple.com/us/app/google+/id447119634",
        "playstore": "https://play.google.com/store/apps/details?id=com.google.android.apps.plus&hl=en"
    },
    "Evernote": {
        "appstore": "https://itunes.apple.com/us/app/evernote/id281796108",
        "playstore": "https://play.google.com/store/apps/details?id=com.evernote&hl=en",
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/evernote/db21927d-f292-e011-986b-78e7d1fa76f8"
    },
    "Amazon Kindle": {
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/amazon-kindle/48195fb4-ee0e-e011-9264-00237de2db9e",
        "playstore": "https://play.google.com/store/apps/details?id=de.amazon.mShop.android&hl=en",
        "appstore": "https://itunes.apple.com/us/app/id348712880"
    },
    "CNN": {
        "appstore": "https://itunes.apple.com/us/app/cnn-app-for-ipad/id407824176?mt=8",
        "playstore": "https://play.google.com/store/apps/details?id=com.cnn.mobile.android.phone&hl=en",
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/cnn/16d73639-0539-4fc6-91ca-d2987181f381"
    },
    "Twitter":{
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/twitter/0b792c7c-14dc-df11-a844-00237de2db9e",
        "playstore": "https://play.google.com/store/apps/details?id=com.twitter.android",
        "appstore": "https://itunes.apple.com/us/app/id333903271"
    },
    "Amazon modile": {
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/amazon-mobile/351decc7-ea2f-e011-854c-00237de2db9e",
        "playstore": "https://play.google.com/store/apps/details?id=com.amazon.windowshop",
        "appstore": "https://itunes.apple.com/us/app/amazon-mobile/id335187483"
    },
    "Groupon": {
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/groupon/47878950-105f-e011-81d2-78e7d1fa76f8",
        "playstore": "https://play.google.com/store/apps/details?id=com.groupon",
        "appstore": "https://itunes.apple.com/us/app/groupon/id352683833"
    },
    "Shazam": {
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/shazam/2f8d5271-2b81-e011-986b-78e7d1fa76f8",
        "playstore": "https://play.google.com/store/apps/details?id=com.shazam.android",
        "appstore": "https://itunes.apple.com/us/app/shazam-encore/id337288863"
    },
    "Pnadora": {
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/pandora/de2df279-485d-49bb-b53e-3f6a2a9401c1",
        "playstore": "https://play.google.com/store/apps/details?id=com.pandora.android&hl=de",
        "appstore": "https://itunes.apple.com/us/app/pandora-radio/id284035177"
    },
    "Ebay": {
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/ebay/92d3a3a3-66d9-df11-a844-00237de2db9e",
        "playstore": "https://play.google.com/store/apps/details?id=com.ebay.mobile",
        "appstore": "https://itunes.apple.com/us/app/ebay/id282614216"
    },
    "Netflix": {
        "windowsphonestore": "http://www.windowsphone.com/en-us/store/app/netflix/c3a509cd-61d6-df11-a844-00237de2db9e",
        "playstore": "https://play.google.com/store/apps/details?id=com.netflix.mediaclient&hl=en",
        "appstore": "https://itunes.apple.com/us/app/netflix/id363590051"
    }
}


def debug_spider_factory(superclass):
    factory = superclass()

    class DebugSpider(BaseSpider):
        name = 'debug_' + factory.name
        allowed_domains = factory.allowed_domains
        start_urls = [urls[factory.name] for urls in apps.values() if factory.name in urls]

        def parse(self, response):
            return factory.parse_app(response)

    return DebugSpider

DebugPlaystore = debug_spider_factory(playstore.Playstore)
DebugAppstore = debug_spider_factory(appstore.Appstore)
DebugWinStore = debug_spider_factory(windowsphonestore.Windowsphonestore)
