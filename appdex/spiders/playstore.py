from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule

from appdex.items import AppdexItem

from urlparse import urlparse, urlunparse, parse_qsl
from urllib import urlencode

import re

def kick_query_components(url, exceptions=[]):
    r = urlparse(url)
    qsl = parse_qsl(r[4])
    query = urlencode([q for q in qsl if q[0] in exceptions])
    return urlunparse((r[0], r[1], r[2], "", query, ""))

def link_to_english(value):
    if '?' in value:
        value += '&hl=en'
#     print('link %s' % value)
    return value

class Playstore(CrawlSpider):
    name = 'playstore'
    allowed_domains = ['play.google.com']
    start_urls = ['http://play.google.com/store/apps']

    rules = (
        Rule(SgmlLinkExtractor(allow=r'/store/apps/details', process_value=link_to_english),
             callback='parse_app', follow=True),
    )

    def parse_app(self, response):
        hxs = HtmlXPathSelector(response)
        i = AppdexItem()
        i['storeId'] = 'GOOG'
        i['url'] = kick_query_components(response.url, ["id"])
        # find app name
        name = hxs.select("//h1[@class='doc-banner-title']/text()")
        i['name'] = name.extract()[0]
        # find app description
        desc = hxs.select("//div[@id='doc-original-text']").select("*|text()")
        i['desc'] = "".join(desc.extract())


        # find price of app (0.00 of for free)
        price = hxs.select("//span[@class='buy-button-price']/text()").extract()[0]
        price = re.search("\d+,\d+", price)
        if price is not None:
            price = price.group(0)
            price = str(price).replace(',','.')
        else:
            price = "0.00"
        i['price'] = str(price)
        # find id of category
        categoryIdElement = hxs.select("//dl[@class='doc-metadata-list']/dd[5]/a/@href").extract()[0]
        categoryId = re.search("[A-Z_][A-Z_]*", categoryIdElement).group(0)
        i['categoryId'] = categoryId
        # find name of category
        categoryName = hxs.select("//dl[@class='doc-metadata-list']/dd[5]/a/text()").extract()[0]
        i['categoryName'] = categoryName
        # find name of category
        appIconLink = hxs.select("//div[@class='doc-banner-icon']/img/@src").extract()[0]
        i['appIconLink'] = appIconLink
        # find app id
        appId = re.search("id=(\w[\w.]*)", response.url).group(1)
        i['appId'] = appId
        # find app screenshots
        appScreenshots = hxs.select("//div[@class='screenshot-image-wrapper goog-inline-block lightbox']/@data-baseurl").extract()
        i['appScreenshots'] = appScreenshots
        # find rating
        rating = hxs.select("//div[@class='average-rating-value']/text()").extract()[0]
        rating = rating.replace(',','.')
        rating = float(rating) if self.is_number(rating) else 0.00
        i['numberOfStars']=str(rating)
        # find producer id
        producerId = hxs.select("//a[@class='doc-header-link']/@href").extract()[0]
        producerId = re.search("id=(.*)", producerId).group(1)
        i['producerId'] = producerId
        producerName = hxs.select("//a[@class='doc-header-link']/text()").extract()[0]
        i['producerName'] = producerName


        return i

    @staticmethod
    def is_number(s):
        try:
            float(s)
            return True
        except ValueError:
            return False