# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field


class AppdexItem(Item):
    name = Field()
    url = Field()
    desc = Field()
    price = Field()
    categoryId = Field()
    categoryName = Field()
    appIconLink = Field()
    appId = Field()
    appScreenshots = Field()
    numberOfStars = Field()
    producerId = Field()
    producerName = Field()
    storeId = Field()