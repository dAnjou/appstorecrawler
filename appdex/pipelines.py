# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html

import json

from scrapy import log

from redis import StrictRedis

from appdex import settings


class AppdexPipeline(object):

    red = None

    def open_spider(self, spider):
        self.red = StrictRedis()

    def process_item(self, item, spider):
        self.red.publish(settings.REDIS_CHANNEL, json.dumps(dict(item)))
        log.msg("Pipeline: item published '%s'" % item.get('name'),
            level=log.WARNING)
        return item



#class JsonWriterPipeline(object):
#
#    def __init__(self):
#        self.file = open('items.jl', 'wb')
#
#    def process_item(self, item, spider):
#        line = json.dumps(dict(item)) + "\n"
#        self.file.write(line)
#        return item