from celery import Celery
from redis import StrictRedis

from datetime import datetime
from time import sleep

celery = Celery('worker', broker='redis://localhost:6379/0')
red = StrictRedis()

@celery.task
def search(term=None):
    for i in xrange(10):
        sleep(1)
        now = datetime.now().replace(microsecond=0).time()
        red.publish('test', u'%s' % now.isoformat())