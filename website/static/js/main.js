(function($) {

    var $results;
    var $search;
    var $notice;
    var lastSearchTerm = "";
    var currentStore = null;

    $(document).ready(function() {
        $results = $("#results");
        $notice = $("#notice");
        $search = $("#search").val("");

        $search.bind("keyup change", function(e) {
            var searchTerm = $search.val();
            if ($.trim(searchTerm).length == 0) {
                clearResults();
            }
            else if (e.which == 13) {
                prepareSearch(searchTerm);
            }
            else {
                setTimeout(function() {
                    prepareSearch(searchTerm);
                }, 1000);
            }
        });

        var $storeChooser = $("#store-chooser");
        $storeChooser.find("a").click(function() {
            var selectedStore = $(this).attr("data-id");
            if (selectedStore === undefined) selectedStore = null;
            $storeChooser.find(".store-text").html($(this).html());
            if (selectedStore != currentStore) {
                currentStore = selectedStore;
                if ($.trim(lastSearchTerm).length > 0) search(lastSearchTerm);
            }
        });
    });

    function prepareSearch(searchTerm) {
        if (searchTerm == $search.val()) {
            if (lastSearchTerm != searchTerm) {
                $notice.hide();
                lastSearchTerm = searchTerm;
                search(lastSearchTerm);
            }
        }
    }

    function search(searchTerm) {
        var url = "/search/" + searchTerm;
        if (currentStore != null) url += "/store/" + currentStore;
        $.ajax({
            url: url
        }).done(function(data) {
            console.log(data);
            if (data.term == lastSearchTerm) {
                addResults(data.results);
            }
        });
    }

    function addResults(results) {
        $results.html("");
        var $row = null;
        $.each(results, function() {
            if ($row != null && this.similarity === undefined) $row = null;
            if ($row == null) $row = $('<div class="clearfix"></div>').appendTo($results);
            $row.append(createResultItemHtml(this));
        });
    }

    function createResultItemHtml(item) {
        var storeUrl = "/static/images/";
        if (item.storeId == 1) storeUrl += "playstore.jpeg";
        else if (item.storeId == 2) storeUrl += "phonestore.jpeg";
        else if (item.storeId == 3) storeUrl += "appstore.jpeg";

        var $tmp = $('<div data-id="' + item.id + '" class="search-item">\
                <div class="item-navigation clearfix">\
                    <div class="store-image"><img src="' + storeUrl + '" /></div>\
                    ' + ((item.similarity !== undefined) ? '<div class="voting-icon"><i class="icon-arrow-down"></i></div>\
                    <div class="voting-icon"><i class="icon-arrow-up"></i></div>\
                    <div class="item-similarity">Similarity: <span>' + (Math.round(item.similarity * 10000) / 100) + '%</span></div>' : '') + '\
                </div>\
                <div class="clearfix">\
                    <div class="item-icon"><img src="' + item.appIconLink + '" /></div>\
                    <div class="item-details">\
                        <div class="item-name"><a href="' + item.url + '"><strong>' + item.name + '</strong></a></div>\
                        <div>' + item.producerName + '</div>\
                        <div class="item-rate"></div>\
                        <div class="toggle-description" data-collapsed="true"><span class="show-hide">Show</span> description</div>\
                    </div>\
                </div>\
                <div class="item-description">' + item.desc + '</div>\
            </div>');
        $tmp.find(".item-rate").raty({
            score: item.numberOfStars,
            readOnly: true,
            path: "/static/lib/jquery.raty/img"
        });
        $tmp.find(".toggle-description").click(function() {
            var $this = $(this);
            var collapsed = $this.prop("data-collapsed");
            if (!collapsed) {
                $tmp.find(".item-description").slideDown();
                $this.find(".show-hide").text("Hide");
            }
            else {
                $tmp.find(".item-description").slideUp();
                $this.find(".show-hide").text("Show");
            }
            $this.prop("data-collapsed", !collapsed);
        });
        return $tmp;
    }

    function clearResults() {
        $notice.show();
        $results.html("");
        lastSearchTerm = "";
    }

})(jQuery);